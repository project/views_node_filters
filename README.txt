CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Maintainers


INTRODUCTION
------------

Views node filter adds node related filters to views.


REQUIREMENTS
------------

 * Views


INSTALLATION
------------

1. Install as usual, see https://www.drupal.org/node/895232 for further
   information.


MAINTAINERS
-----------

Current maintainers:
* Albert Jankowski (albertski) - https://www.drupal.org/u/albertski
