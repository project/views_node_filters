<?php

/**
 * @file
 * Definition of views_node_filters_handler_filter_node_title_select.
 */

/**
 * Filter by node id.
 *
 * @ingroup views_filter_handlers
 */
class views_node_filters_handler_filter_node_title_select extends views_handler_filter_in_operator {

  /**
   * {@inheritdoc}
   */
  public function has_extra_options() {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function extra_options_form(&$form, &$form_state) {
    parent::extra_options_form($form, $form_state);

    $options = [];
    $node_types = node_type_get_types();
    foreach ($node_types as $node_type) {
      $options[$node_type->type] = $node_type->name;
    }

    // Create form element with options retrieved from database.
    $form['node_types'] = [
      '#title' => t('Content Types'),
      '#type' => 'checkboxes',
      '#options' => $options,
      '#required' => TRUE,
      '#default_value' => isset($this->options['node_types']) ? $this->options['node_types'] : [],
    ];

    $form['node_status'] = [
      '#title' => t('Node Status'),
      '#type' => 'radios',
      '#options' => [
        'any' => 'Any',
        NODE_PUBLISHED => 'Published',
        NODE_NOT_PUBLISHED => 'Unpublished',
      ],
      '#required' => TRUE,
      '#default_value' => isset($this->options['node_status']) ? $this->options['node_status'] : NODE_NOT_PUBLISHED,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function option_definition() {
    $options = parent::option_definition();
    $options['node_types'] = ['default' => ''];
    $options['node_status'] = ['default' => NODE_PUBLISHED];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function get_value_options() {
    if ($this->options['node_types']) {
      $result = db_query('SELECT nid, title FROM node WHERE type IN (:types) AND status IN (:status) order by title', [
        ':types' => array_keys(array_filter($this->options['node_types'])),
        ':status' => $this->options['node_status'] !== 'any' ? $this->options['node_status'] : [NODE_PUBLISHED, NODE_NOT_PUBLISHED],
      ])->fetchAll();

      foreach ($result as $row) {
        $rows[$row->nid] = $row->title;
      }
    }

    $this->value_options = $rows;
    return $rows;
  }

}
