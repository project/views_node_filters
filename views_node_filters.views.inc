<?php

/**
 * @file
 * Provide views handlers and plugins.
 */

/**
 * Implements hook_views_data_alter().
 */
function views_node_filters_views_data_alter(&$data) {
  $data['node']['node_title_select'] = [
    'group' => t('Content'),
    'title' => t('Node Title (select list)'),
    'help' => t('Filter by node title, choosing from dropdown list.'),
    'filter' => [
      'handler' => 'views_node_filters_handler_filter_node_title_select',
    ],
    'real field' => 'nid',
  ];
}
